# Plan Finder

## Goals

Take input in the form of two CSV files:
    1. List of available academic plans and the courses associated with them
    2. List of what courses students have taken and what their declared academic plan is

Produce output:
    1. The top three plans that each student's list of taken courses would fit
    2. Is each student's declared plan in the top three?
    3. The total number of courses in each plan
    4. A total count of how many times item #2 is yes
    5. A rating of how fast the algorithm runs

## How to Install

cd to project root, then install dependencies specified in package.json with the command:
`npm install`

## How to Run

Prepare two CSV files. The first should be of the form:
1415, ABCDEFGH, 1234, Y
1415, QWERTYUI, 4321, Y
1415, HGFEDCBA, 9876
...
The columns are: catalog year, the academic plan, course ID, and Y if required for that plan.

The second should be of the form:
1234567, 212121, 40, ABCDEFGH
7654321, 312132, 30, QWERTYUI
1726354, 970863, 30, HGFEDCBA
...
The columns are: empl ID, course ID, academic level, and academic plan.

Assuming the first CSV file is `file1.csv` and the second is `file2.csv`, run this command:
`node main.js file1.csv file2.csv`

