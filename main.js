var _ = require("lodash");
var Converter = require("csvtojson").core.Converter;
var fs = require("fs"); 
var csvConverter1 = new Converter();

// Append summary information after the last line
// This makes the output no longer a valid CSV file
var SHOW_SUMMARY = false;

// The number of top plans to consider when seeing if the declared plan is among those predicted
var TOP_COUNT = 5;

// Check for the required number of command line arguments
if (process.argv.length < 4) {
    println("Usage: node main.js plans.csv students.csv");
    process.exit(1);
}

// Assign values based on command line arguments
var planCsv = process.argv[2];
var studentCsv = process.argv[3];

//
// Return the CRSE_ID of every course taken, given JSON student data and an EMPLID
//
function findCoursesTaken(studentsJson, student) {
    return _.pluck(_.filter(studentsJson, function(s) {
        return s["EMPLID"] == student;
    }), "CRSE_ID");
}

//
// Return the declared plan for a given student
//
function findDeclaredPlan(studentsJson, student) {
    return _.pluck(_.filter(studentsJson, function(s) {
        return s["EMPLID"] == student;
    }), "ACAD_PLAN")[0];
}

//
// Return the academic level for a given student
//
function findAcadLevel(studentsJson, student) {
    return _.parseInt(_.pluck(_.filter(studentsJson, function(s) {
        return s["EMPLID"] == student;
    }), "ACAD_LEVEL_EOT")[0]);
}

//
// Return the ACAD_PLAN, given the CRSE_ID
//
function findPlanFromCourse(plansJson, course) {
    var result = _.find(plansJson, function(p) {
        return p["CRSE_ID"] == course;
    });
    
    if (result == undefined) {
        return null;
    } else {
        return result["ACAD_PLAN"];
    }
}

//
// Return an array of the courses associated with the given plan
//
function findCoursesForPlan(plansJson, plan) {
    return _.pluck(_.filter(plansJson, function(p) {
        return p["ACAD_PLAN"] == plan;
    }), "CRSE_ID");
}

//
// Return a tally of points in plans as a dictionary, given a list of CRSE_ID's and plan data
//
function tallyPlans(coursesTaken, plansJson) {
    var tallies = new Array();
    _.forEach(coursesTaken, function(course) {
        var plan = findPlanFromCourse(plansJson, course);
        if (plan != null) {
            if (tallies.hasOwnProperty(plan)) {
                tallies[plan] = tallies[plan] + 1;
            } else {
                tallies[plan] = 1;
            }
        }
    });
    return tallies;
}

//
// Return an array of tallies, sorted from biggest to smallest
//
function sortTallies(tallies) {

    // Convert all tallies from an associative array into a normal array so that they can be sorted
    var tallyArray = new Array();
    for (tally in tallies) {
        tallyArray.push({name: tally, value: tallies[tally]});
    }

    return _.sortBy(tallyArray, function(tally) { return tally.value; }).reverse();
}

//
// Convert the academic level (40, 30, 20, or 10) to a string ("Senior", "Junior", etc.)
//
function acadLevelToString(levelNum) {
    var result = "Freshman";
    if (levelNum == 20) {
        result = "Sophomore";
    } else if (levelNum == 30) {
        result = "Junior";
    } else if (levelNum == 40) {
        result = "Senior";
    }
    return result;
}

//
// Display the academic level-specific summary of results
// Valid index values: 0,1,2,3 for fresmen, sophomors, etc.
//
function showAcadSummary(countsByLevel, index) {
    var label = "Freshman";
    if (index == 1) {
        label = "Sophomore";
    } else if (index == 2) {
        label = "Junior";
    } else if (index == 3) {
        label = "Senior";
    }

    println(_.template(label + " total,matching,top: ${total}, ${matchFirst} (${matchFirstProp}), ${matchTop} (${matchTopProp})", {
        "total": countsByLevel[index].total,
        "matchFirst": countsByLevel[index].matchFirst,
        "matchFirstProp": floatToPercent(countsByLevel[index].matchFirst/countsByLevel[index].total),
        "matchTop": countsByLevel[index].matchTop,
        "matchTopProp": floatToPercent(countsByLevel[index].matchTop/countsByLevel[index].total)
    }));
}

//
// Convert a floating point number to a string with a percent sign
//
function floatToPercent(f) {
    result = "" + (f * 100);
    index = result.indexOf(".");
    if (index != -1) {
        result = result.substring(0, index);
    }
    return result + "%";
}

//
// Print a message on the screen
//
function println(message) {
    if (message == undefined) {
        message = "";
    }

    print(message + "\n");
}

//
// Print with no newline
//
function print(message) {
    process.stdout.write(message);
}

// Set up a listener that fires when CSV parsing is done
csvConverter1.on("end_parsed", function(plansJson) {

    // Parse the second CSV file now that the first one is done
    var csvConverter2 = new Converter();
    csvConverter2.on("end_parsed", function(studentsJson) {

        // Both CSV files have now been parsed. Begin calculations.
        var students = _.uniq(_.pluck(studentsJson, "EMPLID"));

        // Print header line of the csv
        print("_ID_, ACAD_LEVEL, ENROLLED_PLAN");
        for (var i=1; i<=TOP_COUNT; i++) {
            print(", PLAN" + i + "_CODE, PLAN" + i + "_IS_MATCH, PLAN" + i + "_TOTAL_COURSES, PLAN" + i + "_MATCHED_COURSES");
        }
        println();

        var totalStudents = 0;    // total number of students in students.csv
        var matchingPlans = 0;    // total number of times a student's declared plan matches with the plan for which they have the most courses
        var topPlanMatchers = 0;  // total number of times a student's declared plan is in the top three of plans that their courses apply to

        // Keep track of values similar to those above, but specific to each academic level
        var countsByLevel = new Array();
        countsByLevel.push({total: 0, matchFirst: 0, matchTop: 0});   // freshmen
        countsByLevel.push({total: 0, matchFirst: 0, matchTop: 0});   // sophomors
        countsByLevel.push({total: 0, matchFirst: 0, matchTop: 0});   // juniors
        countsByLevel.push({total: 0, matchFirst: 0, matchTop: 0});   // seniors

        var studentID = 0;
        _.forEach(students, function(student) {
            studentID++;

            var acadLevel = findAcadLevel(studentsJson, student);
            var coursesTaken = findCoursesTaken(studentsJson, student);
            var tallies = tallyPlans(coursesTaken, plansJson);

            // Turn the associative array of plans and their popularity into a regular array and sort it
            sortedTallies = _.first(sortTallies(tallies), TOP_COUNT);
            var allTopPlans = _.pluck(sortedTallies, "name");
            var declaredPlan = findDeclaredPlan(studentsJson, student);

            // Print a new line in the CSV file
            print(studentID + ", " + acadLevelToString(acadLevel) + ", " + declaredPlan);
            for (var i=0; i<allTopPlans.length; i++) {
                var isMatch = allTopPlans[i] == declaredPlan;
                var courseCount = findCoursesForPlan(allTopPlans[i]).length;
                var matchCount = tallies[allTopPlans[i]];
                if (matchCount == undefined) { matchCount = 0; }
                print(", " + allTopPlans[i] + ", " + isMatch + ", " + courseCount + ", " + matchCount);
            }
            println();

            // Increment the appropriate counters if the student's declared plan is in the top three, or is the top
            totalStudents += 1;
            if (allTopPlans.length > 0) {
                if (allTopPlans[0] == declaredPlan) {
                    matchingPlans += 1;
                }
                if (allTopPlans.indexOf(declaredPlan) != -1) {
                    topPlanMatchers += 1;
                }
            }

            // Increment academic level-specific counters
            if (isNaN(acadLevel) == false) {
                var index = (acadLevel / 10) - 1;      // acad level 40 is at index 3 of the array, etc.
                countsByLevel[index].total += 1;
                if (allTopPlans[0] == declaredPlan) {
                    countsByLevel[index].matchFirst += 1;
                }
                if (allTopPlans.indexOf(declaredPlan) != -1) {
                    countsByLevel[index].matchTop += 1;
                }
            }
        });

        // Provide summary information about all students
        if (SHOW_SUMMARY) {
            println("");
            println("Total students: " + totalStudents);
            println("Students with plan that matches: " + matchingPlans);
            println("Proportion of students with matching plans: " + floatToPercent(matchingPlans/totalStudents));
            println("Students with plan that is in the top " + TOP_COUNT + ": " + topPlanMatchers);
            println("Proportion of students with plan that is in the top " + TOP_COUNT + ": " + floatToPercent(topPlanMatchers/totalStudents));
            println("");
            for (var i=0; i<4; i++) {
                showAcadSummary(countsByLevel, i);
            }
        }
    });
    fs.createReadStream(studentCsv).pipe(csvConverter2);
});

// Kick off parsing of the first CSV file, execution continues when the "end_parsed" event occurs
fs.createReadStream(planCsv).pipe(csvConverter1);

